<?php
/**
 * @copyright	Copyright (C) 2008 - 2012 Özer Kavak -> www.AnlamsalAg.com , All rights reserved.
 * @license	GNU General Public License version 2 or later.
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin');
 
class plgSystemTwitterCard extends JPlugin {

    function plgTwitterCard(&$twtcards, $conf)     
      {
        parent::__construct($twtcards, $conf);

        $this->_plugin = JPluginHelper::getPlugin( 'system', 'TwitterCard' );
        $this->_params = new JParameter( $this->_plugin->params );
      }
    function onAfterRender() 
      {
        //TODO Test all conditions
	//$id= JRequest::getInt('id');
        $id = (JRequest::getVar('view')==='article' && JRequest::getVar('option')==='com_content')? JRequest::getInt('id') : 0;
	$doc =& JFactory::getDocument();
        $buffer = JResponse::getBody();
	$db = JFactory::getDBO();
	$query = " SELECT ".$db->nameQuote('images')." FROM ".$db->nameQuote('#__content')." WHERE ".$db->nameQuote('id')." = ".$db->quote($id).";";
	$db->setQuery($query);
	$imageResult = $db->loadResult();
	$images = json_decode($imageResult);
	if (isset($images->image_fulltext) and !empty($images->image_fulltext))
	    {
	      $image=htmlspecialchars(JURI::root().$images->image_fulltext);
	      $timage='<meta name="twitter:image" value="'.$image.'">';
	    }
	else
	    {
	      $timage='<meta name="twitter:image" value="'.$this->params->get('tdimage','').'">';
	    }
        $tsummary = '<meta name="twitter:card" value="summary">';
	$turl='<meta name="twitter:url" value="'.JURI::current().'">';
	$ttitle='<meta name="twitter:title" value="'.$doc->getTitle().'">';
	$tdesc='<meta name="twitter:description" value="'.$doc->getDescription().'">';
	if ($this->params->get('tcreator','')!='')
	    {
	      $tcreator='<meta name="twitter:creator" value="'.$this->params->get('tcreator','').'">';
	    }

	if ($this->params->get('tsitename','')!='')
	    {
	$tsitename='<meta name="twitter:site" value="'.htmlspecialchars($this->params->get( 'tsitename', '' )).'">';
	    }
	$sp="\n";
	$tdata=$tsummary.$sp.$turl.$sp.$ttitle.$sp.$tdesc.$sp.$timage.$sp.$timage2.$sp.$tcreator.$sp.$tsitename;
        $buffer = preg_replace ("/<\/head>/", "\n".$tdata."</head>", $buffer);
        JResponse::setBody($buffer);
        return true;
      }
}
?>