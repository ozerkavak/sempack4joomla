<?php 
/**
 * @package	Joomla.Semantic
 * @subpackage	Module
 * @copyright	Copyright (C) 2005 - 2010 Ozer Kavak, Inc. All rights reserved.
 * @license	License GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;
class modSparqlHelper
{
 var $resultCount;
 var $variables = array();
 var $mainContent = array();
 var $connError = false;
 var $allVariables = array();

  public function EndpointSelectQueryXml($url,$query,$viewerrors)
      {
	$ct = curl_init();
	curl_setopt($ct, CURLOPT_URL, $url."?query=".$query);
	curl_setopt($ct, CURLOPT_HEADER, 0);
	curl_setopt($ct, CURLOPT_HTTPHEADER, array("Accept: application/sparql-results+xml, application/rdf+xml, */*;q=0.5"));
	curl_setopt($ct, CURLOPT_HTTPGET, 1);
	curl_setopt($ct, CURLOPT_CONNECTTIMEOUT,5);
	curl_setopt($ct, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ct, CURLOPT_FAILONERROR,$viewerrors);
	$endpointSelectQueryXml=curl_exec($ct);  //execute the connection and get response
	if(curl_errno($ct))
	  {
	    $this->connError=True;
	      if($viewerrors==1)
	      {
		echo 'Error: '.curl_error($ct);
	      }
	  }
//	else {$this->connError=FALSE;}
	curl_close($ct);
	return $endpointSelectQueryXml;
        unset($endpointSelectQueryXml);
     }
//For sparql query responses... generates a DOM document object from the sparql/xml response of the endpoint
  public function parseEndpointResponse($response)
    {
	  if ($this->connError==True) 
	    {
	    $doc = new DOMDocument();
	    return $doc;
	    }
	  else
	    {
	    $doc = new DOMDocument();
	    $doc->loadXML($response, LIBXML_NOBLANKS);
	    return $doc;
	    }
    }
  public function getResults($params)
    {
      $urltosparql=JURI::current(); 
      $serverUrl=$params->get('server');
      $sparqlQ=urlencode($params->get('spq'));
      $viewerrors=$params->get('viewerrors');
      $sparqlQ = str_replace ("CURRENT_URL" , $urltosparql , $sparqlQ);
      $response=modSparqlHelper::EndpointSelectQueryXml($serverUrl, $sparqlQ, $viewerrors);
      $queryResults=modSparqlHelper::parseEndpointResponse($response);      
      $results = $queryResults->getElementsByTagName ("result");
      return $results;
    }
  public function parseResults($results)
    {
      $this->resultCount = $results->length; //count number of results
      $i=0;
      foreach ($results as $result) 
	{
	  $bindings=$result->getElementsByTagName ("binding"); //get list of <binding> nodes
	    foreach ($bindings as $binding)
	      {
		$this->allVariables[] = $binding->getAttribute("name");
		$currentVar=$binding->getAttribute("name");
		$this->mainContent[$i][$currentVar]['tagname']=$binding->firstChild->nodeName;
		$this->mainContent[$i][$currentVar]['value']=$binding->firstChild->nodeValue;
                $this->mainContent[$i][$currentVar]['datatype']=$binding->firstChild->getAttribute("datatype");
		$currentlang=$binding->firstChild->getAttribute("xml:lang");
		if ($currentlang===""){$currentlang="en";}
		$this->mainContent[$i][$currentVar]['lang']=$currentlang;
	      }
	  $i++;
	}
      $this->variables=array_values(array_unique($this->allVariables));
    }
}

