<?php
 
/**
 * @package     Joomla.Semantic
 * @subpackage  Module
 * @copyright   Copyright (C) 2011 - 2012 Ozer Kavak. All rights reserved.
 * @license     License GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die;

JLoader::register('modSparqlHelper', JPATH_BASE.'/modules/mod_sparql/helper.php');
require JModuleHelper::getLayoutPath('mod_sparql', $params->get('layout', 'default'));
include_once ('helper.php');
?>
