<?php
 
/**
 * @package     Joomla.Semantic
 * @subpackage  Module
 * @copyright   Copyright (C) 2011 - 2012 Ozer Kavak. All rights reserved.
 * @license     License GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;
?>
<div class="sem-mod<?php echo $moduleclass_sfx ?>"> 
<?php
$a= new modSparqlHelper;
$results = $a->getResults($params);
$a->parseResults($results);
   print "<div><ul class='latestnews'>";  //Lists all items one by one in a list. 
      for ($i=0; $i<$a->resultCount;$i++)
	{
	  for ($j=0;$j<count($a->variables);$j++)
	    {
	      print "<li>".$a->mainContent[$i][($a->variables[$j])]['value']."</li>";
	    }
	}
    print "</ul></div>";
unset($results,$a);      
?>
</div>