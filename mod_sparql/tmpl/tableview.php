<?php
 
/**
 * @package     Joomla.Semantic
 * @subpackage  Module
 * @copyright   Copyright (C) 2011 - 2012 Ozer Kavak. All rights reserved.
 * @license     License GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;
?>
<div class="sem-mod<?php echo $moduleclass_sfx ?>"> 
<?php
$a= new modSparqlHelper;
$results = $a->getResults($params);
$a->parseResults($results);

      print "<div><table>";
	for ($i=0; $i<count($a->variables);$i++)
	 {
	  print "<th>".$a->variables[$i]."</th>";
	 }
	for ($i=0; $i<$a->resultCount;$i++)
	 {
	  print "<tr>";
	    for ($j=0;$j<count($a->variables);$j++)
	      {
		print "<td>".$a->mainContent[$i][($a->variables[$j])]['value']."</td>";
	      }
	  print "</tr>";
	 }
      print "</table></div>";
unset($results,$a);      
?>
</div>