<?php
 
/**
 * @package     Joomla.Semantic
 * @subpackage  Module
 * @copyright   Copyright (C) 2011 - 2012 Ozer Kavak. All rights reserved.
 * @license     License GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;
?>
<div class="sem-mod<?php echo $moduleclass_sfx ?>"> 
<?php
$a= new modSparqlHelper;
$results = $a->getResults($params);
$a->parseResults($results);
   print "<div><ul class='latestnews'>";  //Lists of links and titles if Query results contains just "URL" and "Link Title" (sequential)
      for ($i=0; $i<$a->resultCount;$i++)
	{
	      print "<li><a href='".$a->mainContent[$i][($a->variables[0])]['value']."'>".$a->mainContent[$i][($a->variables[1])]['value']."</a></li>";
	}
    print "</ul></div>";
unset($results,$a);      
?>
</div>
